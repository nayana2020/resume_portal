import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import { useSpring, animated } from 'react-spring/web.cjs'; // web.cjs is required for IE 11 support
import CircularProgress from '@material-ui/core/CircularProgress';
import { ReactComponent as Success } from '../assets/images/tick.svg';
import { ReactComponent as Fail } from '../assets/images/close.svg';
import CloseIcon from '@material-ui/icons/Close';
import { Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: "end"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[10],
    padding: '100px 300px',
    textAlign:'center'
  },
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
  icon: {
    width:'70px',
    height:'70px'
  }
}));

const Fade = React.forwardRef(function Fade(props, ref) {
  const { in: open, children, onEnter, onExited, ...other } = props;
  const style = useSpring({
    from: { opacity: 0 },
    to: { opacity: open ? 1 : 0 },
    onStart: () => {
      if (open && onEnter) {
        onEnter();
      }
    },
    onRest: () => {
      if (!open && onExited) {
        onExited();
      }
    },
  });

  return (
    <animated.div ref={ref} style={style} {...other}>
      {children}
    </animated.div>
  );
});

Fade.propTypes = {
  children: PropTypes.element,
  in: PropTypes.bool.isRequired,
  onEnter: PropTypes.func,
  onExited: PropTypes.func,
};

export default function Loading(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClose = () => {
    props.callbackFn()
    setOpen(false);
  };

  useEffect(() => {
    setOpen(true);
  }, [])

  return (
    <div>
      <Modal
        aria-labelledby="spring-modal-title"
        aria-describedby="spring-modal-description"
        className={classes.modal}
        open={open}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
          <h2 id="spring-modal-title">{props.message}</h2>
          {props.status === 'wait' ? <CircularProgress disableShrink /> : props.status === 'success' ? <Success className={classes.icon} /> : <Fail className={classes.icon}/>}
          </div>
          {props.status === 'success' || props.status === 'fail' ? <div style={{backgroundColor:'white'}}>
            <Button onClick={handleClose} variant="contained" style={{color:"red"}}><CloseIcon/></Button></div> : ''}
          {/* <Button variant="contained" style={{color:"red"}}>close</Button> */}
        </Fade>
      </Modal>
    </div>
  );
}