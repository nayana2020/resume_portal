import React, { Component } from "react";
import Dropzone from "./dropzone/Dropzone";
import "./Upload.css";
import Progress from "./progress/Progress";
import axios from 'axios'
import Paper from '@material-ui/core/Paper';
import checkIcon from '../assets/images/baseline-check_circle_outline-24px.svg'
import { Button } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Loading from "./Loading";
import Alert from '@material-ui/lab/Alert';
import DatePicker from '../Common/DatePicker'
import Popover from "../Common/CommonPopover";
import Title from "../Search/componnents/Title";

class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      files: [],
      uploading: false,
      uploadProgress: {},
      successfullUploaded: false,
      open:false,
      emptyDateError: false,
      dateErrorMessage:'',
      emptyFileError:false,
      fileErrorMessage:'',
      successAlert: false,
      status: 'wait',
      message:'Please wait',
      csvDate: '',
      popoverOpen: false
    };

    this.popoverRef = React.createRef();
    this.dateRef = React.createRef();

    this.onFilesAdded = this.onFilesAdded.bind(this);
    this.renderActions = this.renderActions.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.pickDate = this.pickDate.bind(this);
  }

  onFilesAdded(files) {
    this.setState({
      files: files,
      fileErrorMessage: '',
      emptyFileError: false
    });
  }

  pickDate(date) {
    console.log('hello '+date)
    this.setState({
      csvDate: date,
      emptyDateError: false,
      dateErrorMessage: ''
    })
  }

  closeLoadingModal = () => {
    console.log("close call back")
    this.setState({
      open: false,
      status: 'wait',
      message:'Please wait'
  })
  }

  onSubmit() {
    console.log(this.state.files)
    if(this.state.files.length === 0){
      this.setState({
        emptyFileError: true,
        fileErrorMessage: 'Please select a file'
      })
      return;
    }
    var formData = new FormData();
    for (const key of Object.keys(this.state.files)) {
        const file = this.state.files[key];
        const mimeType = file.name.split(/\.(?=[^.]*$)/)[1];
        if(mimeType === 'csv' && this.state.csvDate === ''){
          this.setState({
            dateErrorMessage:'Please select a date to proceed',
            emptyDateError: true
          })
          this.dateRef.current.focus();
          return
        }
        formData.append('file', file)
    }
    formData.append('date', this.state.csvDate)
    this.setState({
        open: true,
        status: 'wait',
        message:'Please wait'
    })
    axios.post("http://127.0.0.1:8000/resumeparser/parseFile", formData, {
    }).then(res => {
        console.log(res.data)
        // setTimeout(() => {
        //   this.setState({
        //     open: false,
        // })
        // }, 2000)
        this.setState({
          successAlert: true,
          files:[],
          status:'success',
          message:'Successfully uploaded',
          csvDate: '',
        })
        this.dateRef.current.value = '';
    }).catch(error => {
      console.log(error.message)
      // setTimeout(()=>{
      //   this.setState({
      //     open: false,
      //   })
      // },2000)
      this.setState({
        errorAlert: true,
        errorMessage: error.message,
        files:[],
        status:'fail',
        message: error.message,
        csvDate: ''
      })
      this.dateRef.current.value='';
    })
}

  renderProgress(file) {
    const uploadProgress = this.state.uploadProgress[file.name];
    if (this.state.uploading || this.state.successfullUploaded) {
      return (
        <div className="ProgressWrapper">
          <Progress progress={uploadProgress ? uploadProgress.percentage : 0} />
          <img
            className="CheckIcon"
            alt="done"
            src={checkIcon}
            style={{
              opacity:
                uploadProgress && uploadProgress.state === "done" ? 0.5 : 0
            }}
          />
        </div>
      );
    }
  }

  renderActions() {
    if (this.state.successfullUploaded) {
      return (
        <button
          onClick={() =>
            this.setState({ files: '', successfullUploaded: false })
          }
        >
          Clear
        </button>
      );
    } else {
      return (
        <Button
        variant="contained"
        color="primary"
        disabled={this.state.files.length < 0 || this.state.uploading}
        onClick={this.onSubmit}
        startIcon={<CloudUploadIcon />}
      >
        Upload
      </Button>
      );
    }
  }

  render() {
    return (
      <>
      {this.state.open ? <Loading status={this.state.status} message={this.state.message} callbackFn={this.closeLoadingModal}/> : ''}
      <div style={{textAlign:'center',width:'100%'}}>
        <Paper elevation={3} className="Upload">
          <Title>Upload docx/csv files</Title>
          <Alert style={{width:'100%'}} variant="outlined" severity="warning">
            <b>Note:</b>
            <div style={{display:'flex'}}>
              <p>If you are uploading a CSV file please choose a date</p>
              <div>
                <DatePicker ref={this.dateRef} onPickDate={this.pickDate}/>
                { this.state.emptyDateError ? <p style={{color:'red'}}>{this.state.dateErrorMessage}</p> : ''}
              </div>
            </div>
          </Alert>
          <div className="Content">
            <div>
              <Dropzone
                onFilesAdded={this.onFilesAdded}
                disabled={this.state.uploading || this.state.successfullUploaded}
              />
            </div>
            <Paper className="Files">
              {this.state.files.map(file => {
                return (
                  <div key={file.name} className="Row">
                    <span className="Filename">{file.name}</span>
                    {this.renderProgress(file)}
                  </div>
                );
              })}
            </Paper>
          </div>
          <p style={{color:'red'}}>{this.state.fileErrorMessage}</p>
          <div className="Actions">{this.renderActions()}</div>
        </Paper>
      </div>
      </>
    )
  }
}

export default Upload;