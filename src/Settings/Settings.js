import React, { Component }  from 'react';
import Grid from '@material-ui/core/Grid';
import { Theme,makeStyles,createStyles,withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { green } from '@material-ui/core/colors';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import axios from 'axios';
import Switch from '@material-ui/core/Switch';

class Settings extends Component{
    constructor(props) {
      super(props);
      this.state = {
        error: null,
        graphs: [{}]
      }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentDidMount() {
      const apiUrl = 'http://127.0.0.1:8000/resumeparser/getAllGraphs';

      fetch(apiUrl)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              graphs: result
            });
          },
          (error) => {
            this.setState({ error });
          }
        )
    }
     handleInputChange (event){
        let graphs1 = this.state.graphs
          graphs1.forEach(graph => {
           if (graph.graph_id === event.target.value)
              graph.status =  event.target.checked
           })
        this.setState({graphs: graphs1})

   }
  handleSubmit(event){


    event.preventDefault();
    fetch("http://127.0.0.1:8000/resumeparser/updateGraphs", {
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "accept": "application/json"
      },
      "body": JSON.stringify(this.state.graphs)
    })
    .then(response => response.json())
  }
    // const requestOptions = {
    //     method: 'POST',
    //     headers: { 'Content-Type': 'application/json' },
    //     body: JSON.stringify(this.state.graphs)
    // };
    // fetch('http://127.0.0.1:8000/resumeparser/updateGraphs', requestOptions)
    //     .then(response => response.json())
    //   .then(data => console.log(data));
    //
    //   //  .then(data => this.setState({ graphs: data }));
    // }

  render(){
    return(
      <React.Fragment>
        <Typography component="h1" variant="h5" color="primary"  >
          Graph Settings
        </Typography>
          <Grid container spacing={1}>
            <Grid item xs={12}>
                 {this.state.graphs.map(item => (
                   <Typography component={'ul'} variant='body2' align='left' key={item.graph_id}>
                     <FormControl component="fieldset">
                            <FormGroup aria-label="position" row>
                              <FormControlLabel
                                value={item.graph_id}
                                control={<Switch color="primary" />}
                                label={item.graph_name}
                                labelPlacement="end"
                                onChange={this.handleInputChange}
                                checked={item.status}
                              />
                            </FormGroup>
                    </FormControl>
                  </Typography>
               ))}
               <Typography component={'ul'} variant='body2' align='left' >
                  <Button  variant="contained" color="primary" onClick={this.handleSubmit}>
                   Apply
                  </Button>
                </Typography >

             </Grid>
          </Grid>
      </React.Fragment>
    )
  }



}
export default Settings;
