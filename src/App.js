import React from 'react';
import Layout from './Layout/Layout';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import SignIn from './Login/Login'

import Technology from './addTechnology/Technology';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path='/signin'>
          <SignIn/>
        </Route>
        <Route exact path='/addTechnology/Technology' component="Technology">
         <Technology />

        </Route>
        <Route exact path=''>
          <div className="App">
            <Layout/>
          </div>
        </Route>
      </Switch>
      

    </Router>
  );
}

export default App;
