import React, { useState, useEffect } from "react";
import Title from "./Title";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Grid from "@material-ui/core/Grid";
import Axios from "axios";
import { STATIC_URL } from "../../Common/StaticContent";

export default function SearchItem(props) {
  const [inputText, setInputText] = useState("");
  const [skillSet, setSkillSet] = useState([]);
  const [selectedSkills, setSelectedSkills] = useState([]);
  const [validationMsg, setValidationMsg] = useState('');
  const [error, setError] = useState(false);

  useEffect(() => {
    Axios.get(STATIC_URL + "/resumeparser/TechnologyAutoComplete", {
      params: { term: inputText },
    }).then(
      (res) => {
        res.data === "" ? setSkillSet([]) : setSkillSet([{label: "&", value: 0, mode: 0}, {label: ",", value: 0, mode: 0}, ...res.data]);
      },
      (err) => {
        console.log(err);
      }
    );
  }, [inputText]);

  const onClickSubmit = () => {
    clearValidationMessage()
    if (inputText === "" && selectedSkills.length === 0 || selectedSkills[selectedSkills.length-1].value === 0) {
      setValidationError('Please select one search parameter')
      return;
    }
    props.inputFromUser(handleAndOrOperation, selectedSkills);
    setInputText("");
    setSkillSet([]);
    setSelectedSkills([]);
  };

  const handleAndOrOperation = () => {
    let newInputString = "";
    for(let i=0;i<selectedSkills.length;i++){
      if(selectedSkills[i].label === '&'){
        if(i === 1){
          newInputString = newInputString + selectedSkills[i-1].label
          newInputString = newInputString + ' & ';
          newInputString = newInputString + selectedSkills[i+1].label
        }else{
          newInputString = newInputString + ' & ' + selectedSkills[i+1].label
        }
      }
      else if(selectedSkills[i].label === ','){
        if(i === 1){
          newInputString = newInputString + selectedSkills[i-1].label
          newInputString = newInputString + ' , ';
          newInputString = newInputString + selectedSkills[i+1].label
        }else{
          newInputString = newInputString + ' , ' + selectedSkills[i+1].label
        }
      }
    }
    // console.log(newInputString);
    return newInputString
  }

  const clearValidationMessage = () => {
    setError(false);
    setValidationMsg('');
  }

  const setValidationError = (msg) => {
    setError(true);
    setValidationMsg(msg)
  }

  const onSelectSkill = (event, skills) => {
    // console.log("t", selectedSkills);
    setInputText("");
    clearValidationMessage()
    const lastElementIndex = skills.length - 1;
    if(skills.length > 2 && skills[lastElementIndex-2].value !== 0 && skills[lastElementIndex].value === 0){
      skills.pop();
      setValidationError(`'&' or ',' operation not permitted here`);
    }
    setSelectedSkills(skills);
  };

  return (
    <React.Fragment>
      <Title>Search for Employee</Title>
      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item xs={10}>
          <Autocomplete
            multiple
            className="pding"
            id="combo-box-demo"
            // freeSolo
            options={skillSet || []}
            getOptionLabel={(option) => option.label}
            style={{ width: "100%", paddingleft: "500px" }}
            onChange={onSelectSkill}
            renderInput={(params) => (
              <><TextField
                style={{ padding: 24 }}
                id="searchInput"
                placeholder="Enter Skill ,empid you wanna search for ..[&=AND][,=OR]"
                value={inputText}
                {...params}
                fullWidth
                onChange={(e) => {
                  setInputText(e.target.value);
                }}
              />
              { error ? <p style={{textAlign:'center', color:'red'}}>{validationMsg}</p> : '' }
              </>
            )}
          />
        </Grid>
        <Grid item xs={2}>
          <Button variant="contained" color="primary" onClick={onClickSubmit}>
            Search
          </Button>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
