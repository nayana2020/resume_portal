import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Title from "./Title";
import { Button } from "@material-ui/core";
import Axios from "axios";
import LoadingContent from "../../Common/LoadingContent";
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  formControl: {
    margin: theme.spacing(3),
  },
}));
let departmentList = [];
let visaList = [];
export default function SearchFilters(props) {
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    Axios.get("http://127.0.0.1:8000/resumeparser/filterParameters").then(
      (res) => {
        departmentList = res.data.department_info;
        visaList = res.data.visa_info;
        setLoading(false);
      },
      (err) => {
        console.log(err);
      }
    );
  });

  let checkedDeptList = props.checkedList.departments;
  let checkedVisaList = props.checkedList.visa;

  const handleDevList = (event) => {
    if (event.target.checked) {
      checkedDeptList.push(event.target.value);
    } else {
      checkedDeptList = checkedDeptList.filter((checkedVal) => {
        return checkedVal !== event.target.value;
      });
    }
  };
  const handleVisaList = (event) => {
    if (event.target.checked) {
      checkedVisaList.push(event.target.value);
    } else {
      checkedVisaList = checkedVisaList.filter((checkedVal) => {
        return checkedVal !== event.target.value;
      });
    }
  };
  const onApplyFilter = () => {
    props.filterFromUser(checkedDeptList, checkedVisaList);
  };
  const onClearFilter = () => {
    checkedDeptList = [];
    checkedVisaList = [];
    setLoading(true);
    props.filterFromUser(checkedDeptList, checkedVisaList);
  };
  return (
    <React.Fragment>
      <Title>Filter</Title>

      {loading ? (
        <LoadingContent />
      ) : (
        <>
          <Button color="primary" variant="contained" onClick={onApplyFilter}>
            Apply Filter
          </Button>
          <div className={classes.root}>
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel component="legend">
                <Title>Departments</Title>
              </FormLabel>
              <FormGroup>
                {departmentList.map((deptName, index) => {
                  return (
                    <FormControlLabel
                      key={index}
                      control={
                        <Checkbox
                          name={deptName}
                          value={deptName}
                          onChange={handleDevList}
                        />
                      }
                      label={deptName.toString().toUpperCase()}
                    />
                  );
                })}
              </FormGroup>
              <FormLabel component="legend" color="primary">
                <Title>Visa</Title>
              </FormLabel>
              <FormGroup>
                {visaList.map((visa, index) => {
                  return (
                    <FormControlLabel
                      key={index}
                      control={
                        <Checkbox
                          name={visa}
                          value={visa}
                          onChange={handleVisaList}
                        />
                      }
                      label={visa.toString().toUpperCase()}
                    />
                  );
                })}
              </FormGroup>
            </FormControl>
          </div>
          <Button color="secondary" variant="contained" onClick={onClearFilter}>
            Reset
          </Button>
        </>
      )}
    </React.Fragment>
  );
}
