import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import Title from "./Title";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import AssignmentIndIcon from "@material-ui/icons/AssignmentInd";
import PhoneIcon from "@material-ui/icons/Phone";
import PublicIcon from "@material-ui/icons/Public";
import BookmarkIcon from "@material-ui/icons/Bookmark";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import DescriptionIcon from "@material-ui/icons/Description";
import AssistantIcon from "@material-ui/icons/Assistant";
import SearchPagination from "./SearchPagination";
import { Link } from "react-router-dom";
import LoadingContent from "../../Common/LoadingContent";
import { Divider } from "@material-ui/core";
import './css/search-result-style.css';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "flex-start",
    color: theme.palette.text.primary,
    alignItems: "flex-start",
    display:'flex'
  },
  paperHead: {
    padding: theme.spacing(1),
    textAlign: "left",
    color: theme.palette.text.primary,
    fontSize: 20
  },
  paperDescription: {
    padding: theme.spacing(1),
    textAlign: "left",
    color: theme.palette.text.primary,
  },
}));

const resultHeader = ({ spell_correction_info }) => {
  return (
    <>
      {spell_correction_info === null ? null : (
        <h5>
          <i>
            {" "}
            Did you mean <u>{spell_correction_info}</u>? Showing results of{" "}
            <u>{spell_correction_info}</u>
            ...
          </i>
        </h5>
      )}
    </>
  );
};

export default function SearchResult(props) {
  const classes = useStyles();
  let loading = props.loading;
  const setPageNo = (pageNo) => {
    props.pageNoFromUser(pageNo);
  };
  const rows = props.searchResult.employee_list;
  const currentPageNo = props.searchResult.page_info.page_no;
  const totalPages =
    props.searchResult.page_info.last_page ||
    Math.ceil(props.searchResult.count_info / 10);
  const firstIndex = (currentPageNo - 1) * 10 + 1;
  const lastIndex =
    totalPages === currentPageNo
      ? (currentPageNo - 1) * 10 + (props.searchResult.count_info % 10)
      : currentPageNo * 10;
  // const columns = [
  //   "ibs_empid",
  //   "emp_name",
  //   "designation",
  //   "contact",
  //   "functional_group",
  //   "visa",
  //   "visa_location",
  //   "about",
  // ];

  return (
    <React.Fragment>
      <Title>Result</Title>
      {loading ? (
        <LoadingContent />
      ) : rows.length !== 0 ? (
        <>
          {resultHeader(props.searchResult)}
          <h3>
            Showing Result {firstIndex} - {lastIndex} of {props.searchResult.count_info}
          </h3>
          {/* <Table padding="checkbox">
            <TableBody> */}
            <Grid container spacing={3}>
              {rows.map((row, index) => (
                <Grid className='glower' item xs={12}>
                <Link

                  style={{ textDecoration: "none", width:'100%' }}
                  target="_blank"
                  to={"/profile/" + row.ibs_empid}
                >
                  {/* <TableRow key={index}> */}
                    <Paper elevation={6} square>
                      <Grid container item xs={12}>
                        <Grid className={classes.paperHead} xs={12}>
                            <b>{row.emp_name}</b>
                        </Grid>
                      </Grid>
                      <Divider/>
                      <Grid container item xs={12} spacing={1}>
                        <Grid item xs={4}>
                          <Paper elevation={0} className={classes.paper}>
                            {" "}
                            <AssignmentIndIcon /> {row.ibs_empid}
                          </Paper>
                        </Grid>
                        <Grid item xs={4}>
                          <Paper elevation={0} className={classes.paper}>
                            {""}
                            <BookmarkIcon />
                            {row.designation}
                          </Paper>
                        </Grid>
                        <Grid item xs={4}>
                          <Paper elevation={0} className={classes.paper}>
                            <PhoneIcon />
                            {""} {row.contact}
                          </Paper>
                        </Grid>
                        <Grid item xs={4}>
                          <Paper elevation={0} className={classes.paper}>
                            <AssistantIcon />
                            {row.functional_group}
                          </Paper>
                        </Grid>
                        <Grid item xs={4}>
                          <Paper elevation={0} className={classes.paper}>
                            <CheckBoxOutlineBlankIcon /> {row.visa}
                          </Paper>
                        </Grid>
                        <Grid item xs={4}>
                          <Paper elevation={0} className={classes.paper}>
                            {" "}
                            <PublicIcon /> {row.visa_location}
                          </Paper>
                        </Grid>
                      </Grid>
                      <Grid container item xs={12} spacing={1}>
                        <Grid item xs={12}>
                          <Paper
                            elevation={0}
                            className={classes.paperDescription}
                          >
                            <DescriptionIcon />
                            {row.about}
                          </Paper>
                        </Grid>
                      </Grid>
                    </Paper>
                    
                  {/* </TableRow> */}
                </Link>
                </Grid>
              ))}
              </Grid>
            {/* </TableBody>
          </Table> */}
          <hr />
          <h3>
            Showing Result {firstIndex} - {lastIndex} of {props.searchResult.count_info}
          </h3>
          {totalPages > 1 ? (
            <SearchPagination
              currentPageNo={currentPageNo}
              pageCount={totalPages}
              setPageNo={setPageNo}
            />
          ) : null}
        </>
      ) : (
        <h3>No Data Found!!! Please Try With Different Technology.</h3>
      )}
    </React.Fragment>
  );
}
