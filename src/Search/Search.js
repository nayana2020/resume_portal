import React, { useEffect, useState } from "react";
import SearchItem from "./componnents/SearchItem";
import SearchResult from "./componnents/SearchResult";
import SearchFilters from "./componnents/SearchFilters";
import { Grid, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Axios from "axios";
import { STATIC_URL } from "../Common/StaticContent";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
}));

export default function Search() {
  const classes = useStyles();
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  const [loading, setLoading] = useState(true);

  const [searchText, setSearchText] = useState("");
  const [searchFilters, setSearchFilters] = useState({
    departments: [],
    visa: [],
  });
  const [pageNo, setPageNo] = useState(1);
  const [searchResult, setSearchResult] = useState({});
  const [skill_ids, setSkill_ids] = useState([]);

  const inputFromUser = (text, skillSet) => {
    if (text === undefined) {
      return;
    }
    let list = [];
    skillSet.forEach((element) => {
      list.push(element.value);
    });
    setSearchText(text);
    setSkill_ids(list);
    setPageNo(1);
  };
  const filterFromUser = (devList, visaList) => {
    let filters = { departments: devList, visa: visaList };
    setSearchFilters(filters);
  };
  const pageNoFromUser = (pageNo) => {
    setPageNo(pageNo);
  };

  useEffect(() => {
    let param = {
      page_no: pageNo,
      segment_no: 1,
      skill_ids: skill_ids,
      search_text: searchText,
      departments: searchFilters.departments,
      visa: searchFilters.visa,
    };
    setLoading(true);
    Axios.get(STATIC_URL + "/resumeparser/searchEmployees", {
      params: param,
    }).then(
      (res) => {
        console.log("Paramiters:", param);
        console.log(res.data);
        setSearchResult(res.data);
        setLoading(false);
      },
      (error) => {
        console.log(error);
      }
    );
  }, [searchText, searchFilters, pageNo, skill_ids]);

  return (
    <>
      <Grid
        container
        spacing={2}
        direction="row"
        justify="center"
        alignItems="flex-start"
      >
        {/* search area*/}
        <Grid item xs={12} lg={12}>
            <SearchItem inputFromUser={inputFromUser} />
        </Grid>

        {/* search results*/}
        {searchText !== "" || skill_ids.length !== 0 ? (
          <>
            <Grid item xs={12} sm={8} lg={9}>
              <Paper className={fixedHeightPaper}>
                <SearchResult
                  pageNoFromUser={pageNoFromUser}
                  searchResult={searchResult}
                  loading={loading}
                />
              </Paper>
            </Grid>
            {/* search filters*/}
            <Grid item xs={12} sm={4} lg={3}>
              <Paper className={fixedHeightPaper}>
                <SearchFilters
                  filterFromUser={filterFromUser}
                  checkedList={searchFilters}
                />
              </Paper>
            </Grid>
          </>
        ) : null}
      </Grid>
    </>
  );
}
