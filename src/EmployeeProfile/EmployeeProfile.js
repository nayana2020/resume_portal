import React, {useState, useEffect} from 'react';
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from '@material-ui/core';

import Details from './components/Details';
import Profile from './components/Profile'

import { useParams} from "react-router";

import axios from 'axios';
import Skills from './components/Skills';
import {LoadingEmployeeProfile} from '../Common/LoadingContent';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
    // padding: '16px',
  }
}));

const EmployeeProfile = () => {
  const classes = useStyles();
  let { empId } = useParams();
  const [employeeData, setEmployeeData] = useState({})
  const [skills, setSkills] = useState([]);
  const [loading, setLoading] = useState(false);

const setResponseToState = response => {
  setLoading(false);
  setEmployeeData(response.data.empdata.length !== 0 ? response.data.empdata[0] : {});
  setSkills(response.data.context.length !==0 ? response.data.context[0].children:[]);
}

  useEffect(() => {
    setLoading(true);
    axios.get(`http://127.0.0.1:8000/resumeparser/getUserProfile?id=${empId}`)
    .then(response => setResponseToState(response))
    .catch(err => console.error(err))
  }, [])

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={4}
      >
        <Grid
          item
          lg={5}
          md={5}
          xl={6}
          xs={12}
        >
          {/* <LoadingEmployeeProfile/> */}
          <Profile loading={loading} empdata={employeeData}/>
        </Grid>
        <Grid
          item
          lg={7}
          md={7}
          xl={6}
          xs={12}
        >
          <Details loading={loading} empdata={employeeData} />
        </Grid>
      </Grid>
      <Grid
        container
        spacing={4}
      >
        <Grid
          item
          xs={12}
        >
          <Skills loading={loading} skills={skills}/>
        </Grid>
      </Grid>
    </div>
  );
};

export default EmployeeProfile;
