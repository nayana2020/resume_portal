import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
  TextField
} from '@material-ui/core';
import LoadingContent from '../../Common/LoadingContent';


const useStyles = makeStyles(() => ({
  root: {}
}));

const Details = props => {
  const { className,empdata,loading, ...rest } = props;

  const classes = useStyles();

  const states = [
    {
      value: 'alabama',
      label: 'Alabama'
    },
    {
      value: 'new-york',
      label: 'New York'
    },
    {
      value: 'san-francisco',
      label: 'San Francisco'
    }
  ];

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      {loading ? <LoadingContent/> : (<><CardHeader
        title="About"
      />
      <Divider />
      <CardContent>
        <Grid
          container
          spacing={3}
        >
          <Grid item>
            {empdata.about}
          </Grid>
        </Grid>
      </CardContent>
      <Divider /></>)}
    </Card>
  );
};

Details.propTypes = {
  className: PropTypes.string
};

export default Details;
