import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import { makeStyles } from "@material-ui/core/styles";
import PhoneIcon from '@material-ui/icons/Phone';
import LanguageIcon from '@material-ui/icons/Language';
import LocalAirportIcon from '@material-ui/icons/LocalAirport';
import { Tooltip,Fade } from '@material-ui/core';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import {
  Card,
  CardActions,
  CardContent,
  Avatar,
  Typography,
  Divider,
  Button,
  LinearProgress,
  Grid
} from '@material-ui/core';
import LoadingContent from '../../Common/LoadingContent';

const useStyles = makeStyles(theme => ({
  root: {},
  details: {
    display: 'flex'
  },
  avatar: {
    marginLeft: 'auto',
    height: 110,
    width: 100,
    flexShrink: 0,
    flexGrow: 0
  },
  items: {
    marginTop: theme.spacing(2),
    display: "flex"

  },
  item:{
    marginLeft:'10px'
  },
  uploadButton: {
    marginRight: theme.spacing(2)
  }
}));

const capitalize = (s) => {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

const Profile = props => {
  const { className, empdata,loading, ...rest } = props;
  // console.log(empdata)
  const classes = useStyles();

  const user = {
    name: 'Shen Zhi',
    city: 'Los Angeles',
    country: 'USA',
    timezone: 'GTM-7',
    avatar: '/images/avatars/avatar_11.png'
  };
  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      { loading ? <LoadingContent/> : (<CardContent>
        <div className={classes.details}>
          <div>
            <Typography
              gutterBottom
              variant="h5"
            >
              {empdata.emp_name}
            </Typography>
            <Typography
              className={classes.locationText}
              color="textSecondary"
              variant="body1"
            >
              {empdata.designation}
            </Typography>
            <Typography
              className={classes.dateText}
              color="textSecondary"
              variant="body1"
            >
              {capitalize(empdata.functional_group)}
            </Typography>
          </div>
          <Avatar
            className={classes.avatar}
            src={user.avatar}
          />
        </div>
        <Grid container>
          <Grid item xs={6}>
            <div className={classes.items}>
            <PhoneIcon/>
            <div className={classes.item}>{empdata.contact}</div>
          </div>
          </Grid>
          <Grid item xs={6}>
            <div className={classes.items}>
            <Tooltip title="Visa" TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} arrow>
              <LocalAirportIcon/>
            </Tooltip>
              <div className={classes.item}>{empdata.visa === 'none' ? empdata.visa : capitalize(empdata.visa)}</div>
            </div>
          </Grid>
          <Grid item xs={6}>
          <div className={classes.items}>
            <Tooltip title="Visa Location" TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} arrow>
              <LocationOnIcon/>
            </Tooltip>
              <div className={classes.item}>{empdata.visa_location === null ? 'Not Available': capitalize(empdata.visa_location)}</div>
            </div>
          </Grid>
        </Grid>
      </CardContent>)}
      <Divider />
    </Card>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;
