import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
});

export default function SkillLevelBar() {
  const classes = useStyles();
  const [progress, setProgress] = React.useState(10);
  let timer;

  return (
      <>
        {progress >= 100 ? clearInterval(timer) : ''}
        <div className={classes.root}>
        <LinearProgress variant="determinate" value={progress}/>
        </div>
    </>
  );
}
