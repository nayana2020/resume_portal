import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import FastfoodIcon from '@material-ui/icons/Fastfood';
import LaptopMacIcon from '@material-ui/icons/LaptopMac';
import HotelIcon from '@material-ui/icons/Hotel';
import RepeatIcon from '@material-ui/icons/Repeat';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import MessageIcon from '@material-ui/icons/Message';
import DeveloperModeIcon from '@material-ui/icons/DeveloperMode';
import StorageIcon from '@material-ui/icons/Storage';
import DataUsageIcon from '@material-ui/icons/DataUsage';
import WebIcon from '@material-ui/icons/Web';
import VisibilityIcon from '@material-ui/icons/Visibility';
import HttpIcon from '@material-ui/icons/Http';
import LanguageIcon from '@material-ui/icons/Language';
import { Divider } from '@material-ui/core';
import LoadingContent from '../../Common/LoadingContent';
import SkillLevelBar from './SkillLevelBar';
import CloudIcon from '@material-ui/icons/Cloud';
import SettingsIcon from '@material-ui/icons/Settings';

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: '6px 16px',
  },
  secondaryTail: {
    backgroundColor: theme.palette.secondary.main,
  },
}));

export default function Skills(props) {
  const classes = useStyles();
  const iconList = {
      "Messaging":<MessageIcon/>,
      "Programming Language":<DeveloperModeIcon/>,
      "database":<StorageIcon/>,
      "ORM":<DataUsageIcon/>,
      "UI":<WebIcon/>,
      "Testing":<VisibilityIcon/>,
      "Webservices":<HttpIcon/>,
      "Web Application":<LanguageIcon/>,
      "Cloud": <CloudIcon/>,
      "Configuration": <SettingsIcon/>
  }

  return (
    <Paper style={{minHeight:"50px"}} elevation={4}>
        {props.loading ? <LoadingContent/> : <>
            <Typography style={{padding:'20px'}} variant='h5'>Skills</Typography>
            <Divider/>
            <Timeline align="alternate">
                {props.skills.map(skillCategory => (
                    <TimelineItem>
                        <TimelineOppositeContent>
                        </TimelineOppositeContent>
                        <TimelineSeparator>
                        <TimelineDot style={{backgroundColor:'darkkhaki'}}>
                            {iconList[skillCategory.name]}
                        </TimelineDot>
                        <TimelineConnector />
                        </TimelineSeparator>
                        <TimelineContent>
                        <Paper elevation={3} className={classes.paper}>
                            <Typography variant="h6" component="h1">
                            {skillCategory.name}
                            </Typography>
                            <ul style={{listStyleType:'none', paddingTop:'20px'}}>
                                {skillCategory.children.map(skills => (
                                    <li style={{paddingBottom: '20px'}}>{skills.name}<SkillLevelBar/></li>
                                ))}
                            </ul>
                        </Paper>
                        </TimelineContent>
                    </TimelineItem>
                ))}
            </Timeline>
        </>}
    </Paper>
  );
}
