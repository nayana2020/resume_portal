  import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { Checkbox, Switch, FormGroup, TableBody, Table, TableRow } from '@material-ui/core';
import Button from '@material-ui/core/Button';





const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  table: {
    minWidth: 800,
  },
  paper: {
    padding: theme.spacing(1),
    
    color: theme.palette.text.secondary,
    textAlign: "center",
    //backgroundColor: "green",
    alignItems: "center" ,
    
  },
  textField: {
    padding: theme.spacing(0),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    width:200,
    
    
  },

  container: {
    padding: theme.spacing(1),
    textAlign: 'flex-start',
    color: theme.palette.text.secondary,
   
  }, 


}));



export default function Add() {
  const classes = useStyles();

  return (

    
    <div className={classes.root} >
      <Grid container spacing={1} >
      
        <Grid item xs={12} sm={9} >
         
        {/* <Paper className={classes.paper}>
      <Button variant="contained" color="primary">
      Add Technologies
  </Button>
      </Paper> */}


<Table padding="checkbox" >
        <TableBody >
          {
            <TableRow >
              <Grid container border={3} spacing={0}>
                  <Grid container item xs={12} spacing={1}>
                  
                  <Grid item xs={12}>
                    <Paper elevation={0} >
                      <center>
                       <Button variant="contained" color="primary" style={{maxWidth: '300px', maxHeight: '40px', minWidth: '80px', minHeight: '30px'}}>
                      <p>Add Technology</p>
                      </Button>
                      </center>
                    </Paper>
                  </Grid>
                </Grid>
                <Grid container item xs={12} spacing={1}>
                  <Grid item xs={5}>
                    <Paper elevation={0} className={classes.container}>
                      <p>Technology:</p>
                    </Paper>
                  </Grid>
                  <Grid item xs={7}>
                    <Paper elevation={0} className={classes.paper}>
                    <TextField  name="Technology" id="outlined-basic" label="Technology" variant="outlined"  size="25"/>
                    </Paper>
                  </Grid>
                </Grid>
                <Grid container item xs={12} spacing={1}>
                  <Grid item xs={5}>
                    <Paper elevation={0} className={classes.container}>
                      <p>Digital:</p>
                    </Paper>
                  </Grid>
                  <Grid item xs={7}>
                    <Paper elevation={0} className={classes.paper}>
                    <TextField name="Digital" id="outlined-basic" label="Digital" variant="outlined" />
                    </Paper>
                  </Grid>
                </Grid>
                <Grid container item xs={12} spacing={1}>
                  <Grid item xs={5}>
                    <Paper elevation={0} className={classes.container}>
                      <p>Catagory:</p>
                    </Paper>
                  </Grid>
                  <Grid item xs={7}>
                    <Paper elevation={0} className={classes.paper}>
                    <TextField  name="Catagory" id="outlined-basic" label="Catagory" variant="outlined"  size="25"/>
                    </Paper>
                  </Grid>
                </Grid>
                <Grid container item xs={12} spacing={1}>
                  <Grid item xs={5}>
                    <Paper elevation={0} className={classes.container}>
                      <p>Status:</p>
                    </Paper>
                  </Grid>
                  <Grid item xs={7}>
                    <Paper elevation={0} className={classes.paper}>
                    <Checkbox name="Status"/>
                    </Paper>
                  </Grid>
                </Grid>
                <Grid container item xs={12} spacing={1}>
                  <Grid item xs={5}>
                    <Paper elevation={0} className={classes.container}>
                      <p>App Frist:</p>
                    </Paper>
                  </Grid>
                  <Grid item xs={7}>
                    <Paper elevation={0} className={classes.paper}>
                    <TextField  name="App First" id="outlined-basic" label="App First" variant="outlined"  size="25"/>
                    </Paper>
                  </Grid>
                </Grid>
               
                <Grid container item xs={12} spacing={1}>
                  <Grid item xs={12}>
                    <Paper elevation={0} className={classes.paper}>
               <center>
                <Button variant="contained" color="secondary">Button </Button>
              </center>
                    </Paper>
                  </Grid>
                </Grid>
              </Grid>
            </TableRow>
          }
        </TableBody>
      </Table>
      
    
        </Grid>
  </Grid>   
    </div>
    
  );
}
