import React from 'react'

import { Grid, Paper, AppBar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Add from './Add';
import AddList from './AddList';
import UserAccountMenu from '../Layout/UserAccountMenu';

const useStyles = makeStyles((theme) => ({
    paper: {
      padding: theme.spacing(2),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column',
    },
    container: {
      padding: theme.spacing(1),
      display: 'center',
      overflow: 'auto',
      flexDirection: 'column',
    },
    }));

export default function Technology() {
    const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
    return (
        <>
          <Grid container spacing={2}>
            
            {/* AddTechnology*/}
            <Grid item xs={12} md={6} lg={6}>
              <Paper className={classes.container}>
                <Add />
              </Paper>
            </Grid>
              {/* Add List*/}
            <Grid item xs={12} md={6} lg={6}>
              <Paper className={fixedHeightPaper}>
                <AddList />
              </Paper>
            </Grid>
          </Grid>  
        </>
    )
}
