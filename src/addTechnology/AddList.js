import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { Checkbox, Switch, FormGroup } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Route } from 'react-router-dom';




const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    table: {
      minWidth: 300,
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: 'center',
      color: theme.palette.text.secondary,
      
    },
    textField: {
      padding: theme.spacing(0),
      textAlign: 'flex-start',
      color: theme.palette.text.secondary,
      width:200,
      
    },
  
    container: {
      padding: theme.spacing(1),
      textAlign: 'flex-start',
      color: theme.palette.text.secondary,
      // display: "flex",
      // flexWrap: "wrap",
      // marginLeft: theme.spacing(1),
      // marginRight: theme.spacing(1),
      // height: 50,
      // width: 100,
      // alignItems: "center"
    }, 
  
  
  }));

  // state = {
  //   technology: []
  // }
  // componentDidMount() {
  //   axios.get(`http://127.0.0.1:8000/resumeparser/addTechnology`)
  //     .then(res => {
  //       const technology = res.data;
  //       this.setState({ technology });
  //     })
  // }


 
  // useEffect(async () => {
  //   const result = await axios(
  //     'http://127.0.0.1:8000/resumeparser/addTechnology',
  //   );
 
  //   setData(result.data);
  // });

  
  function createData(technology, digital, catagory, status, firstapp) {
      return { technology, digital, catagory, status, firstapp };
    }
    
    const rows = [
      createData('java', 'no', 'Technology', 'true', 'programming'),
      createData('python', 'no', 'framework',' false', 'programming'),
      createData('html', 'no', 'framework', 'false', 'ui'),
      createData('ajax', 'no', 'web', 'true', 'other'),
      createData('reactjs', 'no', 'technology', 'true', 'other'),
    ];
  
  
  function AddList() {
    const classes = useStyles();
    
  return (



    <div className={classes.root}>
      <Paper className={classes.paper}>
      <Button variant="contained" color="primary">
      List of the Technologies
  </Button>
      </Paper>
      
<TableContainer component={Paper}>
  <Table className={classes.table} aria-label="simple table">
    <TableHead>
      <TableRow>
        <TableCell>Technology</TableCell>
        <TableCell align="right">is Digital </TableCell>
        <TableCell align="right">Type</TableCell>
        <TableCell align="right">Status</TableCell>
        <TableCell align="right">Catagory</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      {rows.map((row) => (
        <TableRow key={row.technology}>
          <TableCell component="th" scope="row">
            {row.technology}
          </TableCell>
          <TableCell align="right">{row.digital}</TableCell>
          <TableCell align="right">{row.catagory}</TableCell>
          <TableCell align="right">{row.status}</TableCell>
          <TableCell align="right">{row.firstapp}</TableCell>
        </TableRow>
      ))}
    </TableBody>
  </Table>
</TableContainer>

   
    
    </div>
  );
}


export default AddList;