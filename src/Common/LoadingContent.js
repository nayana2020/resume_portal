import React from 'react'
import '../assets/css/loading.css'
import '../assets/css/loading-style.scss'

export default function LoadingContent() {
    return (
        <div className="wrapper">
            <div className="loader">
                <div className="loader-indicator"></div>
            </div>
        </div>
    )
}

export function LoadingEmployeeProfile() {
    return(
        <div id="container">
            <div className="loading-card">
                <div className="top-section">

                <div className="profile-info">
                    <div className="block block--long load-animate"></div>
                    <div className="block block--short load-animate"></div>
                    <div className="block block--short load-animate"></div>
                </div>
                <div className="profile-pic load-animate">
                </div>
                </div>
                <div className="loading-divider"></div>
                <div className="bottom-section">
                <div className="content-header load-animate"></div>
                <div className="content-section">
                    <div className="content-info">
                    <div className="block block--long load-animate"></div>
                    <div className="block block--long load-animate"></div>
                    <div className="block block--long load-animate"></div>
                    <div className="block block--long load-animate"></div>
                    <div className="block block--long load-animate"></div>
                    <div className="block block--long load-animate"></div>
                    <div className="block block--long load-animate"></div>
                    </div>
                    <div className="content-image load-animate"></div>
                </div>
                </div>
            </div>
        </div>
    )
}
