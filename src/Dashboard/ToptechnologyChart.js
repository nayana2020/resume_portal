import React, { Component } from 'react'
import axios from 'axios';
import {Pie} from 'react-chartjs-2';

const options = {
  maintainAspectRatio: true,
  responsive: true,
  legend: {
    display:false,
    position: 'bottom',
    labels: {
      boxWidth: 10
    }
  }
}
const Toptechnology = [ 'Core Java','Hibernate','React JS', 'Spring','DB/Sql','Flutter','J2EE', 'HTML', 'Android','SOAP'];


const colors=['#FF6384',
      '#4BC0C0',
      '#FFCE56',
      '#E7E9ED',
      '#36A2EB'];
export class ToptechnologyChart extends Component{

 constructor(props) {
               super(props);
               this.state = { Data: {} };
   }
   componentDidMount() {
          axios.get('http://127.0.0.1:8000/resumeparser/getAllTechnologies')
                         .then(res => {
                                const employees = res.data;
                                let skillname = Toptechnology;
                                //let skillname = [];
                                let count = [];
                                let bgColors=[];
                                skillname.forEach(record => {
                                    skillname.push(record);
                                    count.push(Math.floor(Math.random() * colors.length));
                                    bgColors.push(colors[Math.floor(Math.random() * colors.length)]);
                                 });
                                // employees.forEach(record => {
                                //     skillname.push(record.region);
                                //     count.push(record.totalCases);
                                //     bgColors.push(colors[Math.floor(Math.random() * colors.length)]);
                                //  });
                       this.setState({
                                        Data: {
                                                labels: skillname,
                                                datasets: [
                                                        {
                                                                data: count,
                                                                backgroundColor: bgColors
                                                        }
                                                ]
                                        }
                                });
                        })
        }

        render() {

               return (

                       <div>

                               <Pie
                                       data={this.state.Data}
                                       options={options}

                               />

                       </div>

               )

       }

}
export default ToptechnologyChart;
