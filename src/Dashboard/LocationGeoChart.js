import React, { memo, useEffect, useState } from "react";
import {
  ComposableMap,
  Geographies,
  Geography
} from "react-simple-maps";

import axios from 'axios';

const geoUrl =
  "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

const rounded = num => {
  if (num > 1000000000) {
    return Math.round(num / 100000000) / 10 + "Bn";
  } else if (num > 1000000) {
    return Math.round(num / 100000) / 10 + "M";
  } else {
    return Math.round(num / 100) / 10 + "K";
  }
};

const LocationGeoChart = ({ setTooltipContent }) => {

  const [locationList, setLocationList] = useState([]);

  useEffect(() => {
   axios.get('http://127.0.0.1:8000/resumeparser/getAllVisaLocations')
   .then(response => setLocationList(response.data.filter(location => location.visa_location !== null)))
   .catch(error => console.log(error))
  },[])

  const getEmpCountByLocation = (locationName, locationAbbr) => {
    locationName = locationName.toLowerCase();
    locationAbbr = locationAbbr.replace(/\./g, '').toLowerCase();
    for(let i=0 ; i < locationList.length ; i++){
      const visaLoc = locationList[i].visa_location.toLowerCase();
      if(locationName === visaLoc || locationAbbr === visaLoc)
        return locationList[i].name_count

    }
    return 0
  }

  return (
    <>
      <ComposableMap data-tip="" height='300' width='800' projectionConfig={{ scale: 90 }}>
          <Geographies geography={geoUrl}>
            {({ geographies }) =>
              geographies.map(geo => (
                <Geography
                  key={geo.rsmKey}
                  geography={geo}
                  onMouseEnter={() => {
                    const { ABBREV, NAME, POP_EST } = geo.properties;
                    setTooltipContent(`${NAME} — ${getEmpCountByLocation(NAME, ABBREV)}`);
                  }}
                  onMouseLeave={() => {
                    setTooltipContent("");
                  }}
                  style={{
                    default: {
                      fill: "#D6D6DA",
                      outline: "none"
                    },
                    hover: {
                      fill: "#F53",
                      outline: "none"
                    },
                    pressed: {
                      fill: "#E42",
                      outline: "none"
                    }
                  }}
                />
              ))
            }
          </Geographies>
      </ComposableMap>
    </>
  );
};

export default memo(LocationGeoChart);
