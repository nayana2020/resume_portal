import React from 'react'
import axios from 'axios';
import { Bubble } from 'react-chartjs-2';



const options = {
        legend: {
            display: false,
            position: 'bottom'
        },
  };

const data = {
  datasets: [
    {
      label: 'Tokyo',
      data: [
        {
          x: 10,
          y: 10,
          r: 40
        }
      ],
      backgroundColor:"black",
      hoverBackgroundColor: "black"
    },
    {
      label: 'Bellvue',
        data: [
          {
            x: 6,
            y: 2,
            r: 30
          }
        ],
        backgroundColor:"yellow",
        hoverBackgroundColor: "yellow"
    },
    {
      label: 'France',
        data: [
          {
            x: 2,
            y: 6,
            r: 25
          }
        ],
        backgroundColor:"silver",
        hoverBackgroundColor: "silver"
    },
    {
      label: 'Kochi',
        data: [
          {
            x: 5,
            y: 3,
            r: 10
          }
        ],
        backgroundColor:"grey",
        hoverBackgroundColor: "grey"
    },
    {
      label: 'Trivandrum',
        data: [
          {
            x: 2,
            y: 1,
            r: 10
          }
        ],
        backgroundColor:"blue",
        hoverBackgroundColor: "blue"
    },
    {
      label: 'Dubai',
        data: [
          {
            x: 15,
            y: 13,
            r: 7
          }
        ],
        backgroundColor:"red",
        hoverBackgroundColor: "red"
    },
    {
      label: 'Chicago',
        data: [
          {
            x: 10,
            y: 5,
            r: 50
          }
        ],
        backgroundColor:"#FFCE56",
        hoverBackgroundColor: "#FFCE56"
    },
    {
      label: 'Singapore',
        data: [
          {
            x: 14,
            y: 12,
            r: 30
          }
        ],
        backgroundColor:"#36A2EB",
        hoverBackgroundColor: "#36A2EB"
    },
    {
      label: 'Banglore',
        data: [
          {
            x: 3,
            y: 5,
            r: 50
          }
        ],
        backgroundColor:"pink",
        hoverBackgroundColor: "pink"
    },
    {
      label: 'Sydni',
        data: [
          {
            x: 4,
            y: 12,
            r: 9
          }
        ],
        backgroundColor:"#36A2EB",
        hoverBackgroundColor: "#36A2EB"
    }
    ]
};



export default function LocationChart()
{
  return(
     <Bubble  data={data}  options={options}/>
  );
};
