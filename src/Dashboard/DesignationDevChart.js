import React, { Component } from "react";
import axios from "axios";
import { Bar } from "react-chartjs-2";

const colors = [
  "aqua",
  "black",
  "blue",
  "fuchsia",
  "gray",
  "green",
  "lime",
  "maroon",
  "navy",
  "olive",
  "orange",
  "purple",
  "red",
  "teal",
  "yellow",
  "pink",
  "brown",
];
const options = {
  responsive: true,
  legend: {
    display: false,
    position: "bottom",
  },
};

class DesignationDevChart extends Component {
  constructor(props) {
    super(props);
    this.state = { Data: {} };
  }
  componentDidMount() {
    axios
      .get("http://127.0.0.1:8000/resumeparser/getDevEmployees")
      .then((res) => {
        const employees = res.data;
        console.log(employees);
        let designation = [];
        let count = [];
        let bgColors = [];
        employees.forEach((record) => {
          designation.push(record.designation);
          count.push(record.total);
          bgColors.push(colors[Math.floor(Math.random() * colors.length)]);
        });
        this.setState({
          Data: {
            labels: designation,
            datasets: [
              {
                data: count,
                backgroundColor: bgColors,
              },
            ],
          },
        });
      });
  }
  render() {
    return (
      <div>
        <Bar data={this.state.Data} options={options} />
      </div>
    );
  }
}
export default DesignationDevChart;
