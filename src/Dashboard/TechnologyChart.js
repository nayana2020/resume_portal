import React, { Component } from 'react'
import axios from 'axios';
import { Bar} from 'react-chartjs-2';


const options = {
        labels:true,
        legend: {
            display: false,
            position: 'bottom',
            labels: {
              boxWidth: 5
            }
        },
  };
const colors=['aqua', 'black', 'blue', 'fuchsia', 'gray', 'green',
            'lime', 'maroon', 'navy', 'olive', 'orange', 'purple', 'red',
             'teal', 'yellow','pink','brown'];

export class TechnologyChart extends Component{

 constructor(props) {
               super(props);
               this.state = { Data: {} };
   }
   componentDidMount() {
          axios.get('http://127.0.0.1:8000/resumeparser/getAllTechnologies')
                         .then(res => {
                                const employees = res.data;
                                let skillname = [];
                                let count = [];
                                let bgColors=[];

                                employees.forEach(record => {
                                    skillname.push(record.technology);
                                    count.push(Math.floor(Math.random() * colors.length));
                                    bgColors.push(colors[Math.floor(Math.random() * colors.length)]);
                                 });
                                 this.setState({
                                   Data:{
                                     labels:skillname,
                                     datasets:[
                                              {
                                                data:count,
                                                backgroundColor:bgColors
                                              }
                                     ]
                                   }
                                 });
                        })
        }

        render() {

               return (

                       <div>

                               <Bar
                                       data={this.state.Data}
                                       options={options}
                               />

                       </div>

               )

       }

}
export default TechnologyChart;
