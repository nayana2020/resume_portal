import React, { Component } from 'react'
import axios from 'axios';
import { Doughnut } from 'react-chartjs-2';

const project = [ 'ANA', 'iCargo','iFlight','MSC', 'Jetstart', 'Hilton','Ifly res','Iport' ];


const colors=['aqua', 'black', 'blue', 'fuchsia', 'gray', 'green',
            'lime', 'maroon', 'navy', 'olive', 'orange', 'purple', 'red',
             'teal', 'yellow','pink','brown'];
 const options = {
         legend: {
             display: false,
             position: 'bottom'
         },
   };

class ProjectChart extends Component{
constructor(props){
  super(props);
  this.state={ Data: {} };
}
componentDidMount(){
  axios.get('http://127.0.0.1:8000/resumeparser/getAllTechnologies')
        .then(res=>{

          const employees=res.data;
          console.log(employees);
          let projectname =project;
          let count = [];
          let bgColors=[];
          projectname.forEach(record => {
              projectname.push(record);
              count.push(Math.floor(Math.random() * colors.length));
              bgColors.push(colors[Math.floor(Math.random() * colors.length)]);
           });
          // employees.forEach(record => {
          //     projectname.push(record.region);
          //     count.push(record.totalCases);
          //     bgColors.push(colors[Math.floor(Math.random() * colors.length)]);
          //  });
           this.setState({
             Data:{
               labels:projectname,
               datasets:[
                        {
                          label:projectname,
                          data:count,
                          backgroundColor:bgColors
                        }
               ]
             }
           });


        })
}
render(){
  return(
    <div>
    <Doughnut data={this.state.Data} options={options} />
    </div>
  )
}
}
export default ProjectChart;
