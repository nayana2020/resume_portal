import React ,{useState}from 'react';
import AppBar from '@material-ui/core/AppBar';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import ToptechnologyChart from './ToptechnologyChart';
import TechnologyChart from './TechnologyChart';
import ProjectChart from './ProjectChart';
import LocationChart from './LocationChart'
import DesignationTestingChart from './DesignationTestingChart'
import DesignationDevChart from './DesignationDevChart'
import LocationGeoChart from './LocationGeoChart';
import ReactTooltip from "react-tooltip";
const useStyles = makeStyles((theme) => ({

  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height:'100%'
  },
  title: {
    flexGrow: 1,
  },
  cardHeader: {
      backgroundColor:
        theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[400],
    },
}));



export default function Dashboard() {
  const classes = useStyles();
  const [content, setContent] = useState("");

  return (
    <React.Fragment>
         {/* <Container className={classes.cardGrid}> */}
          {/* End hero unit */}
          <Grid container spacing={4}>
              <Grid item style={{height:'500px'}}  xs={12} sm={12} md={12} lg={12} xl={12}>
               <Card className={classes.card}>
                 <CardHeader title='DQ across Locations'  className={classes.cardHeader} />
                 {/* <LocationChart/> */}
                 <LocationGeoChart setTooltipContent={setContent}/>
                 <ReactTooltip>{content}</ReactTooltip>
                </Card>
              </Grid>
              <Grid item  xs={12} sm={6} md={6}>
                <Card className={classes.card}>
                <CardHeader title='DQ across Designations-Testing'  className={classes.cardHeader} />
                 <DesignationTestingChart />
                </Card>
              </Grid>
              <Grid item  xs={12} sm={6} md={6}>
                <Card className={classes.card}>
                <CardHeader title='DQ across Project'  className={classes.cardHeader} />
                 <ProjectChart />
                 </Card>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
                <Card className={classes.card}>
                <CardHeader title='DQ across Designations-Development'  className={classes.cardHeader} />
                 <DesignationDevChart />
                </Card>
              </Grid>
              <Grid item  xs={12} sm={6} md={6}>
                <Card className={classes.card}>
                 <CardHeader title='All Technologies'  className={classes.cardHeader} />
                 <TechnologyChart />
                 </Card>
              </Grid>
              <Grid item  xs={12} sm={6} md={6}>
                <Card className={classes.card}>
                 <CardHeader title='Top 10 Technologies'  className={classes.cardHeader} />
                 <ToptechnologyChart />
                 </Card>
              </Grid>

          </Grid>
        {/* </Container> */}


    </React.Fragment>
  );
}
